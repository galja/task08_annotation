package com.epam.controller;

import com.epam.helper.Helper;
import com.epam.model.Employee;
import com.epam.annotations.MyAnnotation;
import com.epam.model.Prize;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Controller {
    private static Logger logger = LogManager.getLogger(Controller.class);
    private Prize prize;
    private Employee employee;
    private Field[] fields;
    private StringBuilder stringBuilder;

    public Controller() {
        stringBuilder = new StringBuilder();
        prize = new Prize();
        employee = new Employee(Helper.EMPLOYEE_NAME, Helper.EMPLOYEE_SURNAME, Helper.EMPLOYEE_AGE, Helper.EMPLOYEE_WAGE);
        fields = employee.getClass().getDeclaredFields();
    }

    public String getFields() {
        for (Field field : fields) {
            Annotation[] annotations = field.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().equals(MyAnnotation.class)) {
                    field.setAccessible(true);
                    try {
                        stringBuilder.append(field.getName()).append("\t")
                                .append(field.get(employee));
                    } catch (IllegalAccessException e) {
                        logger.error("Illegal Access");
                    }
                }
            }
        }
        return stringBuilder.toString();
    }

    public String givePrizeToEmployee(int percent) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method getPrize = prize.getClass().getDeclaredMethod("getPrize", Employee.class, Integer.TYPE);
        Prize prize = new Prize();
        Double sumPrize = (Double) getPrize.invoke(prize, employee, percent);
        return employee.toString() + "Prize: " + sumPrize;
    }

    public String getAnnotationValue() {
        Class cl = employee.getClass();
        for (Field field : cl.getDeclaredFields()) {
            MyAnnotation annotation;
            if ((annotation = field.getAnnotation(MyAnnotation.class)) != null) {
                stringBuilder.append(field.getName()).append("\t").append(annotation.value()).append("\n");
            }
        }
        return stringBuilder.toString();
    }

    public <T> String setEmployeesAge(T newAge) throws NoSuchFieldException, IllegalAccessException {
        String result = Helper.EMPLOYEE + employee.getName() + "\t" + employee.getSurname() + "\t" + employee.getWage();
        Field employeesAge = employee.getClass().getDeclaredField("age");
        result += employeesAge.get(employee).toString();
        employeesAge.setAccessible(true);
        try {
            employeesAge.set(employee, newAge);
        } catch (Exception e) {
            logger.info("Wrong type");
        }
        result += employeesAge.get(employee).toString();
        return result;
    }

    public String getAllInformationAboutClass() {
        return allInformationAboutClass(employee);
    }

    private <T> String allInformationAboutClass(T object) {

        Class unknownClass = object.getClass();
        stringBuilder.append(unknownClass.getSimpleName()).append("\n");
        for (Field field : unknownClass.getDeclaredFields()) {
            stringBuilder.append(field.getName()).append("\t")
                    .append(Arrays.toString(field.getDeclaredAnnotations()))
                    .append("\n");
        }
        for (Method method : unknownClass.getDeclaredMethods()) {
            stringBuilder.append(method.getModifiers()).append("\t")
                    .append(Arrays.toString(method.getParameterTypes()));
        }
        return stringBuilder.toString();
    }
}
