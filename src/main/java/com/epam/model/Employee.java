package com.epam.model;

import com.epam.annotations.MyAnnotation;

public class Employee {

    @MyAnnotation("84")
    private String name;
    private String surname;
    private int age;

    @MyAnnotation("56")
    private int wage;

    public Employee(String name, String surname, int age, int wage) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.wage = wage;
    }

    public int getWage() {
        return wage;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", wage=" + wage +
                '}';
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
}
