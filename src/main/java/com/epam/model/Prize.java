package com.epam.model;

import com.epam.helper.Helper;

public class Prize {

    public double getPrize(Employee employee, int percent) {
        return (double) (employee.getWage() * percent / Helper.HUNDRED_PERCENT);
    }
}
