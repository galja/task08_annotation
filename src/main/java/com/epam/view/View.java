package com.epam.view;

import com.epam.controller.Controller;
import com.epam.helper.Helper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private static Logger logger = LogManager.getLogger(View.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public View() {
        controller = new Controller();
        generateMenu();
        putMethods();
    }

    private void outputMenu() {
        logger.info("\nMENU\n");
        for (String str : menu.values()) {
            logger.info("\n" + str);
        }
        logger.info("Enter Q to exit");
    }

    public void show() {
        Scanner in = new Scanner(System.in);
        String keyMenu;
        do {
            outputMenu();
            logger.info("\nPlease, select menu point:\n");
            keyMenu = in.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.getStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Print fields from Employee class annotated with MyAnnotation");
        menu.put("2", "2 - Print annotation value");
        menu.put("3", "3 - Print employees prize (invoked method)");
        menu.put("4", "4 - Set employee's age (not knowing it's type)");
        menu.put("5", "5 - Print information about class");
    }

    private void putMethods() {
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printFieldsWithAnnotation);
        methodsMenu.put("2", this::printAnnotationValue);
        methodsMenu.put("3", this::printEmployeesPrize);
        methodsMenu.put("4", this::printAge);
        methodsMenu.put("5", this::printInformationAboutClass);
    }

    private void printInformationAboutClass() {
        logger.info(controller.getAllInformationAboutClass());
    }

    private void printAge() {
        try {
            logger.info(controller.setEmployeesAge(Helper.AGE));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void printEmployeesPrize() {
        try {
            logger.info(controller.givePrizeToEmployee(Helper.PERCENTAGE));
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void printAnnotationValue() {
        logger.info(controller.getAnnotationValue());
    }

    private void printFieldsWithAnnotation() {
        logger.info(controller.getFields());
    }
}
