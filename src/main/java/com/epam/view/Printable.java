package com.epam.view;

interface Printable {
    void print();
}
