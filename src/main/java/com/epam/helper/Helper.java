package com.epam.helper;

public class Helper {
    public static final String EMPLOYEE_NAME = "Stepan";
    public static final String EMPLOYEE_SURNAME = "Voituk";
    public static final String EMPLOYEE = "Employee";
    public static final int EMPLOYEE_AGE = 42;
    public static final int EMPLOYEE_WAGE = 900;
    public static final int HUNDRED_PERCENT = 100;
    public static final int PERCENTAGE = 40;
    public static final int AGE = 40;
}
